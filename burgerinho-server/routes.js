const express = require("express");
const router = express.Router();
const MenuItem = require("./models/menuItemSchema");
const BestOffer = require("./models/bestOfferSchema");
const DeliveryOrder = require("./models/deliveryOrderSchema");
const PickupOrder = require("./models/pickupOrderSchema");
const NutritionDetails = require("./models/nutritionDetailsSchema");
const errors = require("./errors/errors");
const { deliveryOrder, pickupOrder } = require("./validations/orderValidation");

router.get("/nutrition-details/:id", async (req, res, next) => {
  try {
    const itemDetails = await NutritionDetails.findOne({ itemId: req.params.id });
    if (!itemDetails) next(errors.MENU_ITEM_NOT_FOUND);
    else res.status(200).send(itemDetails);
  } catch (err) {
    console.log(err);
    next(errors.SERVER_ERROR);
  }
});

router.post("/pickup-orders", pickupOrderValidation, async (req, res, next) => {
  try {
    const newPickupOrder = new PickupOrder(req.body);
    await newPickupOrder.save();
    console.log(newPickupOrder);
    res.status(200).send("Pickup order created!");
  } catch (err) {
    console.log(err);
    next(errors.SERVER_ERROR);
  }
});

router.post("/delivery-orders", deliveryOrderValidation, async (req, res, next) => {
  try {
    const newDeliveryOrder = new DeliveryOrder(req.body);
    await newDeliveryOrder.save();
    console.log(newDeliveryOrder);
    res.status(200).send("Delivery order created!");
  } catch (err) {
    console.log(err);
    next(errors.SERVER_ERROR);
  }
});

router.get("/menuItems", async (req, res, next) => {
  try {
    const food = await MenuItem.find();
    res.status(200).send(food);
  } catch (err) {
    console.log(err);
    next(errors.SERVER_ERROR);
  }
});
router.get("/bestOffers", async (req, res, next) => {
  try {
    const food = await BestOffer.find();
    res.status(200).send(food);
  } catch (err) {
    console.log(err);
    next(errors.SERVER_ERROR);
  }
});
router.get("/foodCategories", async (req, res, next) => {
  try {
    const categories = await MenuItem.distinct("category");
    res.status(200).send(categories);
  } catch (err) {
    console.log(err);
    next(errors.SERVER_ERROR);
  }
});
router.post("/foods", async (req, res, next) => {
  try {
    const food = new MenuItem({
      name: req.body.name,
      category: req.body.category,
      price: req.body.price,
      picture: req.body.picture,
    });
    await food.save();
    res.send(food);
  } catch (err) {
    console.log(err);
    next(errors.SERVER_ERROR);
  }
});

//Functions
function deliveryOrderValidation(req, res, next) {
  const order = deliveryOrder.validate(req.body);
  if (order.error) {
    const VALIDATION_ERROR = {
      code: 40003,
      status: 400,
      message: order.error.details[0].message,
    };

    next(VALIDATION_ERROR);
  } else {
    next();
  }
}
function pickupOrderValidation(req, res, next) {
  const order = pickupOrder.validate(req.body);
  if (order.error) {
    const VALIDATION_ERROR = {
      code: 40003,
      status: 400,
      message: order.error.details[0].message,
    };

    next(VALIDATION_ERROR);
  } else {
    next();
  }
}

module.exports = router;
