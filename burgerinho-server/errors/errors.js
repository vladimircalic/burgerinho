const errors = {
  NOT_FOUND: {
    code: "E40401",
    message: "Resource not found",
    status: 404,
  },
  MENU_ITEM_NOT_FOUND: {
    code: "E40402",
    message: "Selected item not found",
    status: 404,
  },
  SERVER_ERROR: {
    code: "E50001",
    message: "Unexpected server error",
    status: 500,
  },
};

module.exports = errors;
