const joi = require("@hapi/joi");

const validationSchema = {
  deliveryOrder: joi.object({
    orders: joi.array(),
    deliveryAddress: joi.string(),
    phoneNumber: joi.string(),
    deliveryInstructions: joi.string().allow(""),
  }),

  pickupOrder: joi.object({
    orders: joi.array(),
    pickupPlace: joi.string(),
    phoneNumber: joi.string(),
    timeOfArrival: joi.string(),
  }),
};

module.exports = validationSchema;
