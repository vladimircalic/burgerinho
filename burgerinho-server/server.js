const express = require("express");
const app = express();
const path = require("path");
const port = process.env.PORT || 5001;
const mongoose = require("mongoose");
const menuRouter = require("./routes");
const errors = require("./errors/errors");

//App port
app.listen(port, () => console.log(`Burgerinho Server listening on port ${port}...`));

//Database connection
mongoose.connect("mongodb+srv://noliferop:svesuiste1@gymer-app.zscig.mongodb.net/Burgerinho", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
const db = mongoose.connection;
db.on("error", (error) => console.log(error));
db.once("open", () => console.log("Connected to Database Burgerinho"));

//Application
app.use(express.json());
app.use(express.static(path.join(__dirname, "../burgerinho-app/dist")));
app.get("/", (req, res) => {
  res.sendFile("index.html", { root: path.join(__dirname, "../burgerinho-app/dist/") });
});
app.use("/api", menuRouter);
//Invalid route handler
app.use((req, res, next) => {
  next(errors.NOT_FOUND);
});
//Error handler
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.send({
    error: {
      code: err.code,
      message: err.message,
    },
  });
});
