const mongoose = require("mongoose");

const pickupOrderSchema = new mongoose.Schema({
  orders: {
    type: Array,
    required: true,
  },
  phoneNumber: {
    type: String,
    required: true,
  },
  pickupPlace: {
    type: String,
    required: true,
  },
  timeOfArrival: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("pickupOrder", pickupOrderSchema);
