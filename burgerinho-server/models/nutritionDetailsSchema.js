const mongoose = require("mongoose");

const nutritionDetailsSchema = new mongoose.Schema({
  itemId: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("NutritionDetails", nutritionDetailsSchema);
