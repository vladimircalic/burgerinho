const mongoose = require("mongoose");

const deliveryOrderSchema = new mongoose.Schema({
  orders: {
    type: Array,
    required: true,
  },
  deliveryAddress: {
    type: String,
    required: true,
  },
  phoneNumber: {
    type: String,
    required: true,
  },
  deliveryInstructions: {
    type: String,
  },
});

module.exports = mongoose.model("DeliveryOrder", deliveryOrderSchema);
