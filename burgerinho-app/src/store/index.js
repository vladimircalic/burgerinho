import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    errorMessage: null,
    successMessage: null,
  },
  mutations: {
    setErrorMessage(state, payload) {
      state.errorMessage = payload;
      setTimeout(() => {
        state.errorMessage = null;
      }, 3000);
    },
    setSuccessMessage(state, payload) {
      state.successMessage = payload;
      setTimeout(() => {
        state.successMessage = null;
      }, 3000);
    },
  },
  actions: {},
  modules: {},
  getters: {
    setErrorMessage(state) {
      return state.errorMessage;
    },
    setSuccessMessage(state) {
      return state.successMessage;
    },
  },
});
