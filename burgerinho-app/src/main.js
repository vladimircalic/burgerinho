import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "bootstrap/dist/css/bootstrap.css";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { faHamburger } from "@fortawesome/free-solid-svg-icons";
import { faSortDown } from "@fortawesome/free-solid-svg-icons";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import {
  faArrowLeft,
  faArrowAltCircleLeft,
  faWindowClose,
  faExclamationTriangle,
  faTimes,
  faCheck,
} from "@fortawesome/free-solid-svg-icons";
import Vuelidate from "vuelidate";

Vue.use(Vuelidate);

library.add(
  faShoppingCart,
  faHamburger,
  faSortDown,
  faBars,
  faArrowLeft,
  faArrowAltCircleLeft,
  faWindowClose,
  faExclamationTriangle,
  faTimes,
  faCheck
);

Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
